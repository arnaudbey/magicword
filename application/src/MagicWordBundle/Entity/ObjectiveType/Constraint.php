<?php

namespace MagicWordBundle\Entity\ObjectiveType;

use Doctrine\ORM\Mapping as ORM;
use MagicWordBundle\Entity\Objective;

/**
 * Constraint.
 *
 * @ORM\Entity
 * @ORM\Table(name="objective_type_constraint")
 */
class Constraint extends Objective
{
    protected $discr = 'constraint';

    public function getDiscr()
    {
        return $this->discr;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="numberToFind", type="integer")
     */
    private $numberToFind;

    /**
     * @ORM\ManyToOne(targetEntity="LexiconBundle\Entity\Category")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="LexiconBundle\Entity\Gender")
     */
    private $gender;

    /**
     * @ORM\ManyToOne(targetEntity="LexiconBundle\Entity\Tense")
     */
    private $tense;

    /**
     * @ORM\ManyToOne(targetEntity="LexiconBundle\Entity\Person")
     */
    private $person;

    /**
     * @ORM\ManyToOne(targetEntity="LexiconBundle\Entity\Mood")
     */
    private $mood;

    /**
     * @ORM\ManyToOne(targetEntity="LexiconBundle\Entity\Number")
     */
    private $number;

    /**
     * Set category.
     *
     * @param \LexiconBundle\Entity\Category $category
     *
     * @return Constraint
     */
    public function setCategory(\LexiconBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return \LexiconBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set numberToFind.
     *
     * @param int $numberToFind
     *
     * @return Constraint
     */
    public function setNumberToFind($numberToFind)
    {
        $this->numberToFind = $numberToFind;

        return $this;
    }

    /**
     * Get numberToFind.
     *
     * @return int
     */
    public function getNumberToFind()
    {
        return $this->numberToFind;
    }

    /**
     * Set gender.
     *
     * @param \LexiconBundle\Entity\Gender $gender
     *
     * @return Constraint
     */
    public function setGender(\LexiconBundle\Entity\Gender $gender = null)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender.
     *
     * @return \LexiconBundle\Entity\Gender
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set tense.
     *
     * @param \LexiconBundle\Entity\Tense $tense
     *
     * @return Constraint
     */
    public function setTense(\LexiconBundle\Entity\Tense $tense = null)
    {
        $this->tense = $tense;

        return $this;
    }

    /**
     * Get tense.
     *
     * @return \LexiconBundle\Entity\Tense
     */
    public function getTense()
    {
        return $this->tense;
    }

    /**
     * Set person.
     *
     * @param \LexiconBundle\Entity\Person $person
     *
     * @return Constraint
     */
    public function setPerson(\LexiconBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person.
     *
     * @return \LexiconBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set mood.
     *
     * @param \LexiconBundle\Entity\Mood $mood
     *
     * @return Constraint
     */
    public function setMood(\LexiconBundle\Entity\Mood $mood = null)
    {
        $this->mood = $mood;

        return $this;
    }

    /**
     * Get mood.
     *
     * @return \LexiconBundle\Entity\Mood
     */
    public function getMood()
    {
        return $this->mood;
    }

    /**
     * Set number.
     *
     * @param \LexiconBundle\Entity\Number $number
     *
     * @return Constraint
     */
    public function setNumber(\LexiconBundle\Entity\Number $number = null)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number.
     *
     * @return \LexiconBundle\Entity\Number
     */
    public function getNumber()
    {
        return $this->number;
    }

    public function export()
    {
        $jsonArray = array(
            'type' => $this->discr,
            'numberToFind' => $this->numberToFind,
            'category' => $this->category ? $this->category->getId() : null,
            'gender' => $this->gender ? $this->gender->getId() : null,
            'tense' => $this->tense ? $this->tense->getId() : null,
            'person' => $this->person ? $this->person->getId() : null,
            'mood' => $this->mood ? $this->mood->getId() : null,
            'number' => $this->number ? $this->number->getId() : null,
        );

        return $jsonArray;
    }
}
