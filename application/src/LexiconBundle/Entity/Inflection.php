<?php

namespace LexiconBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use JsonSerializable;

/**
 * Inflection.
 *
 * @ORM\Table(name="lexicon.inflection", indexes={
 *  @Index(name="cleaned_content", columns={"cleaned_content"}),
 * })
 * @ORM\Entity(repositoryClass="LexiconBundle\Repository\InflectionRepository")
 */
class Inflection implements JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="LexiconBundle\Entity\Language")
     */
    private $language;

    /**
     * @ORM\ManyToOne(targetEntity="LexiconBundle\Entity\Lemma", cascade={"persist"})
     */
    private $lemma;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=255)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="cleaned_content", type="string", length=255)
     */
    private $cleanedContent;

    /**
     * @ORM\ManyToOne(targetEntity="LexiconBundle\Entity\Number")
     */
    private $number;

    /**
     * @ORM\ManyToOne(targetEntity="LexiconBundle\Entity\Gender")
     */
    private $gender;

    /**
     * @ORM\ManyToOne(targetEntity="LexiconBundle\Entity\Tense")
     */
    private $tense;

    /**
     * @ORM\ManyToOne(targetEntity="LexiconBundle\Entity\Person")
     */
    private $person;

    /**
     * @ORM\ManyToOne(targetEntity="LexiconBundle\Entity\Mood")
     */
    private $mood;

    /**
     * @var string
     *
     * @ORM\Column(name="phonetic1", type="string", length=255)
     */
    private $phonetic1;

    /**
     * @var string
     *
     * @ORM\Column(name="phonetic2", type="string", length=255)
     */
    private $phonetic2;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content.
     *
     * @param string $content
     *
     * @return Inflection
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set phonetic1.
     *
     * @param string $phonetic1
     *
     * @return Inflection
     */
    public function setPhonetic1($phonetic1)
    {
        $this->phonetic1 = $phonetic1;

        return $this;
    }

    /**
     * Get phonetic1.
     *
     * @return string
     */
    public function getPhonetic1()
    {
        return $this->phonetic1;
    }

    /**
     * Set phonetic2.
     *
     * @param string $phonetic2
     *
     * @return Inflection
     */
    public function setPhonetic2($phonetic2)
    {
        $this->phonetic2 = $phonetic2;

        return $this;
    }

    /**
     * Get phonetic2.
     *
     * @return string
     */
    public function getPhonetic2()
    {
        return $this->phonetic2;
    }

    /**
     * Set lemma.
     *
     * @param \LexiconBundle\Entity\Lemma $lemma
     *
     * @return Inflection
     */
    public function setLemma(\LexiconBundle\Entity\Lemma $lemma = null)
    {
        $this->lemma = $lemma;

        return $this;
    }

    /**
     * Get lemma.
     *
     * @return \LexiconBundle\Entity\Lemma
     */
    public function getLemma()
    {
        return $this->lemma;
    }

    /**
     * Set number.
     *
     * @param \LexiconBundle\Entity\Number $number
     *
     * @return Inflection
     */
    public function setNumber(\LexiconBundle\Entity\Number $number = null)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number.
     *
     * @return \LexiconBundle\Entity\Number
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set gender.
     *
     * @param \LexiconBundle\Entity\Gender $gender
     *
     * @return Inflection
     */
    public function setGender(\LexiconBundle\Entity\Gender $gender = null)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender.
     *
     * @return \LexiconBundle\Entity\Gender
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set tense.
     *
     * @param \LexiconBundle\Entity\Tense $tense
     *
     * @return Inflection
     */
    public function setTense(\LexiconBundle\Entity\Tense $tense = null)
    {
        $this->tense = $tense;

        return $this;
    }

    /**
     * Get tense.
     *
     * @return \LexiconBundle\Entity\Tense
     */
    public function getTense()
    {
        return $this->tense;
    }

    /**
     * Set person.
     *
     * @param \LexiconBundle\Entity\Person $person
     *
     * @return Inflection
     */
    public function setPerson(\LexiconBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person.
     *
     * @return \LexiconBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set mood.
     *
     * @param \LexiconBundle\Entity\Mood $mood
     *
     * @return Inflection
     */
    public function setMood(\LexiconBundle\Entity\Mood $mood = null)
    {
        $this->mood = $mood;

        return $this;
    }

    /**
     * Get mood.
     *
     * @return \LexiconBundle\Entity\Mood
     */
    public function getMood()
    {
        return $this->mood;
    }

    /**
     * Set cleanedContent.
     *
     * @param string $cleanedContent
     *
     * @return Inflection
     */
    public function setCleanedContent($cleanedContent)
    {
        $this->cleanedContent = $cleanedContent;

        return $this;
    }

    /**
     * Get cleanedContent.
     *
     * @return string
     */
    public function getCleanedContent()
    {
        return $this->cleanedContent;
    }

    /**
     * Set language.
     *
     * @param \LexiconBundle\Entity\Language $language
     *
     * @return Game
     */
    public function setLanguage(\LexiconBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language.
     *
     * @return \LexiconBundle\Entity\Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'content' => $this->content,
            'cleanedContent' => $this->cleanedContent,
        );
    }
}
