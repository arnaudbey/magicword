# Requirements
* docker-ce :  https://docs.docker.com/install/linux/docker-ce/ubuntu/
* docker-compose
* git lfs: https://packagecloud.io/github/git-lfs/install

# Installation
```
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/lzbk/MagicWord.git
cd MagicWord
cp .env.dist .env
vi .env
sudo docker-compose up --build -d
sudo docker-compose exec php bash
PWD=rootpassword make install
```

# Notes
* When installing docker-ce, if you are using a ubuntu based distro (such as Mint), in step 4 of the ubuntu install replace  ```$(lsb_release -cs) \``` with the actual name of the ubuntu distro your OS is based upon (For Mint sylvia, it would be xenial)
* During the last docker command, you should be able to leave the default value for every parameter (just press enter, except for the last one, the secret phrase, which should be changed)
* if you let default values, MW should be available at localhost:666 and the phpmyadmin at localhost:8080
* You'd better leave default values, some stuff is hardcoded...

* Once application installed, you can generate some grid with these commands in the php container. It will generates 10 grids in french and english with at least 150 (and 250 for french) foundable forms.

> php bin/console magicword:generate-grid english 10 150

> php bin/console magicword:generate-grid french 10 250


# Update
```
git pull origin master
sudo docker-compose up --build -d
sudo docker-compose exec php bash
make update
```
